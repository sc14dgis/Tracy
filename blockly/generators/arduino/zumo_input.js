'use strict';

goog.provide('Blockly.Arduino.zumo_input');

goog.require('Blockly.Arduino');

Blockly.Arduino.zumo_button_wait = function() {
  var dropdown_button_selection = this.getFieldValue('button_selection');
  // TODO: Assemble JavaScript into code variable.
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_lcd'] = "Zumo32U4LCD lcd;";
  
  // define the right button A,B or C
  if (dropdown_button_selection === 'A'){
  	Blockly.Arduino.definitions_['var_buttonA'] = "Zumo32U4ButtonA buttonA;";
  } else if (dropdown_button_selection === 'B'){
  	Blockly.Arduino.definitions_['var_buttonB'] = "Zumo32U4ButtonB buttonB;";
  } else {
  	Blockly.Arduino.definitions_['var_buttonC'] = "Zumo32U4ButtonC buttonC;";
  }
  
  var code = 	'lcd.clear();\n'+
  	'lcd.print(F("Press '+ dropdown_button_selection +'"));\n'+
  	'button'+ dropdown_button_selection +'.waitForButton();\n'+
  	'lcd.clear();\n';

  return code;
};

Blockly.Arduino.zumo_proximity_sensors = function() {
  //var dropdown_button_selection = this.getFieldValue('button_selection');
  // TODO: Assemble JavaScript into code variable.
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
  Blockly.Arduino.definitions_['Zumo32U4ProximitySensors'] = "Zumo32U4ProximitySensors proxSensors;";

  
  Blockly.Arduino.setups_['proximity_sensor_setup'] = 'proxSensors.initThreeSensors();';
  
  var code = 'proxSensors.read();\n';

  return code;
};

Blockly.Arduino.left_prox_sensor = function() {
  Blockly.Arduino.definitions_['Zumo32U4ProximitySensors'] = "Zumo32U4ProximitySensors proxSensors;";
  Blockly.Arduino.setups_['proximity_sensor_setup'] = 'proxSensors.initThreeSensors();';
  // TODO: Assemble JavaScript into code variable.
  var code = 'proxSensors.countsFrontWithLeftLeds()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.zumo_accelerometer = function() {
  //var dropdown_button_selection = this.getFieldValue('button_selection');
  // TODO: Assemble JavaScript into code variable.
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
  Blockly.Arduino.definitions_['Zumo32U4Accelerometer'] = "LSM303 lsm303;";

  Blockly.Arduino.setups_['wire_begin'] = 'Wire.begin();';
  Blockly.Arduino.setups_['accelerometer_init'] = 'lsm303.init();';
  Blockly.Arduino.setups_['accelerometer_default'] = 'lsm303.enableDefault();';
  ;
  var code = 'lsm303.read();\n';

  return code;
};

Blockly.Arduino.accel_x = function() {
  // TODO: Assemble JavaScript into code variable.
  var code = 'lsm303.a.x';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.accel_y = function() {
  // TODO: Assemble JavaScript into code variable.
  var code = 'lsm303.a.y';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Arduino.ORDER_NONE];
};