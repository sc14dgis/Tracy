goog.provide('Blockly.Arduino.zumo_unitTests');

goog.require('Blockly.Arduino');

Blockly.Arduino.run_tests = function() {
    var statements_name = Blockly.Arduino.statementToCode(this, 'loop');

    var code = statements_name;
    console.log(code);
    return code;
};

Blockly.Arduino.test_vs_input = function() {
  console.log("test_vs function entered");
  var value_name = Blockly.Arduino.valueToCode(this, 'Name', Blockly.Arduino.ORDER_ATOMIC);
  var value_actual = Blockly.Arduino.valueToCode(this, 'Actual', Blockly.Arduino.ORDER_ATOMIC);
  var value_expected = Blockly.Arduino.valueToCode(this, 'Expected', Blockly.Arduino.ORDER_ATOMIC);
  
  // TODO: Assemble JavaScript into code variable.
  
  var code = '"'+value_actual+'"';
  //if (value_actual == value_expected){
  //  code = "true";
  //};else{code = "false"}
  if (value_actual == value_expected) {
    code = "PASS";
} else {
    code = "FAIL";
}
  
  //console.log(value_name + ":  "+ code);
  return value_name + ":  "+ code;
};

Blockly.Arduino.test_Text = function() {
  var text_test = this.getFieldValue('Test');
  // TODO: Assemble JavaScript into code variable.
  var code = text_test;
  // TODO: Change ORDER_NONE to the correct strength.
  //console.log(code);
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.statement_Test = function() {
  var value_name = Blockly.Arduino.valueToCode(this, 'test_Name', Blockly.Arduino.ORDER_ATOMIC);
  var value_actual = Blockly.Arduino.statementToCode(this, 'statement_To_Test');
  var value_expected = Blockly.Arduino.valueToCode(this, 'expected_Output', Blockly.Arduino.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  
  var code = "NULL"

  //value_actual = value_actual.replace("\n", "");
  value_actual = value_actual.replace(/\n/g, '');
  value_actual = value_actual.replace(/ /g, '');
  value_actual = '('+value_actual+')';
  //if (value_actual == value_expected){
  //  code = "true";
  //};else{code = "false"}
  if (value_actual == value_expected) {
    code = "PASS";
} else {
    code = "FAIL";
}
  //console.log(value_name + ":  "+ code + "\n");
  return value_name + ":  "+ code + "\n";
};