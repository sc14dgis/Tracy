'use strict';

goog.provide('Blockly.Arduino.zumo_variables');

goog.require('Blockly.Arduino');


Blockly.Arduino.setup_var = function() {
  var variable_var_name = Blockly.Arduino.variableDB_.getName(this.getFieldValue('var_name'), Blockly.Variables.NAME_TYPE);
  var dropdown_var_type = this.getFieldValue('var_type');
  var value_name = Blockly.Arduino.valueToCode(this, 'NAME', Blockly.Arduino.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = dropdown_var_type+ " " +variable_var_name+ " = " +value_name+ ";\n";
  return code;
};