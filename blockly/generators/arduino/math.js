'use strict';

goog.provide('Blockly.Arduino.math');

goog.require('Blockly.Arduino');


Blockly.Arduino.math_number = function() {
  // Numeric value.
  var code = window.parseFloat(this.getFieldValue('NUM'));
  // -4.abs() returns -4 in Dart due to strange order of operation choices.
  // -4 is actually an operator and a number.  Reflect this in the order.
  var order = code < 0 ?
      Blockly.Arduino.ORDER_UNARY_PREFIX : Blockly.Arduino.ORDER_ATOMIC;
  return [code, order];
};

Blockly.Arduino.math_random_int = function() {
  // Numeric value.
  var from = this.getFieldValue('FROM') || '0';
  var to = this.getFieldValue('TO') || '0';
  var code = 'random('+from+','+to+')';

  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.math_change = function() {
  // Numeric value.
  var v = this.getFieldValue('VAR') || '0';
  var delta = this.getFieldValue('DELTA') || '0';
  var code = 'v = v +'+delta+';';

  return code;
};

Blockly.Arduino.math_number_property = function() {

  var property = this.getFieldValue('PROPERTY');
  var number = this.getFieldValue('NUMBER_TO_CHECK') || '0';

  var code;

  if(property == 'EVEN'){
    code = "("+number+" % 2) == 0";
  }else if(property == 'ODD'){
    code = "("+number+" % 2) == 1";
  }else if(property == 'POSITIVE'){
    code = +number+" >= 0";
  }else if(property == 'NEGATIVE'){
    code = number+" <=0";
  }else {
    var s = this.getFieldValue('DIVISIBLE_BY') || '1';
    code = "("+number+" % "+s+") == 0";
  }

  return [code, Blockly.Arduino.ORDER_NONE];
};


Blockly.Arduino.math_arithmetic = function() {
  // Basic arithmetic operators, and power.
  var mode = this.getFieldValue('OP');
  var tuple = Blockly.Arduino.math_arithmetic.OPERATORS[mode];
  var operator = tuple[0];
  var order = tuple[1];
  var argument0 = Blockly.Arduino.valueToCode(this, 'A', order) || '0';
  var argument1 = Blockly.Arduino.valueToCode(this, 'B', order) || '0';
  var code;
  if (!operator) {
    code = 'Math.pow(' + argument0 + ', ' + argument1 + ')';
    return [code, Blockly.Arduino.ORDER_UNARY_POSTFIX];
  }
  code = argument0 + operator + argument1;
  return [code, order];
};

Blockly.Arduino.math_arithmetic.OPERATORS = {
  ADD: [' + ', Blockly.Arduino.ORDER_ADDITIVE],
  MINUS: [' - ', Blockly.Arduino.ORDER_ADDITIVE],
  MULTIPLY: [' * ', Blockly.Arduino.ORDER_MULTIPLICATIVE],
  DIVIDE: [' / ', Blockly.Arduino.ORDER_MULTIPLICATIVE],
  POWER: [null, Blockly.Arduino.ORDER_NONE]  // Handle power separately.
};
