'use strict';

goog.provide('Blockly.Arduino.intermediate_blocks');

goog.require('Blockly.Arduino');

Blockly.Arduino.intermediate_number = function() {
  // Numeric value.
  var code = window.parseFloat(this.getFieldValue('NUM'));
  // -4.abs() returns -4 in Dart due to strange order of operation choices.
  // -4 is actually an operator and a number.  Reflect this in the order.
  var order = code < 0 ?
      Blockly.Arduino.ORDER_UNARY_PREFIX : Blockly.Arduino.ORDER_ATOMIC;
  return [code, order];
};

Blockly.Arduino.intermediate_text = function() {
  // Text value.
  var code = Blockly.Arduino.quote_(this.getFieldValue('TEXT'));
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};



Blockly.Arduino.intermediate_for_loop = function() {
  var value_variable = Blockly.Arduino.valueToCode(this, 'variable_names', Blockly.Arduino.ORDER_ATOMIC);
  //var variable_name = Blockly.Arduino.getFieldValue(this, 'variable_names', Blockly.Arduino.ORDER_ATOMIC);
  //var variable_variable_name = Blockly.Arduino.variableDB_.getName(this.getFieldValue('variable_names'), Blockly.Variables.NAME_TYPE);
  //var variable_variable_name = Blockly.Arduino.variableDB_.getName(this.getFieldValue('NAME'), Blockly.Arduino.NAME_TYPE);
  var value_for = Blockly.Arduino.valueToCode(this, 'for', Blockly.Arduino.ORDER_ATOMIC);
  var value_until = Blockly.Arduino.valueToCode(this, 'until', Blockly.Arduino.ORDER_ATOMIC);
  var value_step = Blockly.Arduino.valueToCode(this, 'step', Blockly.Arduino.ORDER_ATOMIC);
  var statements_loop = Blockly.Arduino.statementToCode(this, 'loop');
  // TODO: Assemble JavaScript into code variable.
  var code = 'for(' +value_variable+ ' = ' +value_for+ '; '+value_variable+ ' < ' +value_until+ '+1; ' +value_variable+ ' = ' +value_variable+ ' + ' +value_step+ '){\n'+
  statements_loop +'\n'+
  '}';
  return code;
};

Blockly.Arduino.intermediate_while_loop = function() {
  var value_while = Blockly.Arduino.valueToCode(this, 'while', Blockly.Arduino.ORDER_ATOMIC);
  var while_statements = Blockly.Arduino.statementToCode(this, 'while_statements');
  // TODO: Assemble JavaScript into code variable.
  var code = 'while('+value_while+'){\n'+
  while_statements+'\n'+
  '}\n';
  return code;
};