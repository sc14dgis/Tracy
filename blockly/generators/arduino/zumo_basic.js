goog.provide('Blockly.Arduino.zumo_basic');

goog.require('Blockly.Arduino');

Blockly.Arduino.basic_forward = function() {
    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
    Blockly.Arduino.definitions_['var_zumo32u4motors'] = "Zumo32U4Motors motors;\n";

    var code = "motors.setSpeeds(200,200);\ndelay(450);\nmotors.setSpeeds(0,0);\n";
    return code;
};

Blockly.Arduino.basic_reverse = function() {
    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
    Blockly.Arduino.definitions_['var_zumo32u4motors'] = "Zumo32U4Motors motors;\n";

    var code = "motors.setSpeeds(-200,-200);\ndelay(450);\nmotors.setSpeeds(0,0);\n";
    return code;
};

Blockly.Arduino.basic_right_turn = function() {
    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
    Blockly.Arduino.definitions_['var_zumo32u4motors'] = "Zumo32U4Motors motors;\n";

    var code = "motors.setSpeeds(200,-200);\ndelay(500);\nmotors.setSpeeds(0,0);\n";
    return code;
};

Blockly.Arduino.basic_left_turn = function() {
    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
    Blockly.Arduino.definitions_['var_zumo32u4motors'] = "Zumo32U4Motors motors;\n";

    var code = "motors.setSpeeds(-200,200);\ndelay(500);\nmotors.setSpeeds(0,0);\n";
    return code;
};

Blockly.Arduino.basic_loop = function() {
    var num_loop_count = this.getFieldValue('loop_count');
    var statements_name = Blockly.Arduino.statementToCode(this, 'statements_in_loop');
    

    var code = "int x;\nfor(x=0; x<" + num_loop_count + "; x++){\n" +statements_name+"}\n";
    return code;
};

Blockly.Arduino.basic_start = function() {
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
  Blockly.Arduino.definitions_['var_lcd'] = "Zumo32U4LCD lcd;";
  Blockly.Arduino.definitions_['var_buttonA'] = "Zumo32U4ButtonA buttonA;";
  var setup_code = 'lcd.clear();\n'+
    'lcd.print(F("Press buttonA"));\n'+
    'buttonA.waitForButton();\n'+
    'lcd.clear();\n';
  Blockly.Arduino.setups_['proximity_sensor_setup'] = setup_code;

  var code = '//Start block used';
  console.log("basic block");
  console.log(code)
  console.log("end basic")
  return code;
};

Blockly.Arduino.basic_end = function() { 
    var code = 'while (true){}\n';
    return code;
};