goog.provide('Blockly.Arduino.zumo');

goog.require('Blockly.Arduino');

Blockly.Arduino.zumo_motors = function() {
    var value_l_motor = Blockly.Arduino.valueToCode(this, 'l_motor', Blockly.Arduino.ORDER_ATOMIC) || '0';
    var value_r_motor = Blockly.Arduino.valueToCode(this, 'r_motor', Blockly.Arduino.ORDER_ATOMIC) || '0';

    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

    Blockly.Arduino.definitions_['var_zumo32u4motors'] = "Zumo32U4Motors motors;\n";
    var code = "motors.setSpeeds("+value_l_motor+"," + value_r_motor + ");\n";
    return code;
};

Blockly.Arduino.zumo_stop = function() {
    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

    Blockly.Arduino.definitions_['var_zumo32u4motors'] = "Zumo32U4Motors motors;\n";
    var code = "motors.setSpeed(0,0);\n";
    return code;
};

Blockly.Arduino.zumo_accel = function() {
  var dropdown_axis = this.getFieldValue('axis');
  
    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

    Blockly.Arduino.definitions_['var_lsm303'] = "LSM303 compass;";

    Blockly.Arduino.setups_['compass_setup'] = 
    'if(!compass.init()){\n'+
    ' while(1){\n'+
    '   Serial.println("Failed to detect gyro.");\n'+
    '   delay(2000);\n'+
    ' }\n'+
    '}\n'+
    "compass.enableDefault();\n"

  var code = 'compass.a.'+dropdown_axis;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.zumo_gyro = function() {
  var dropdown_axis = this.getFieldValue('axis');
  
    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

    Blockly.Arduino.definitions_['var_l3g'] = "L3G gyro;";

    Blockly.Arduino.setups_['gyro_setup'] = 
    'if(!gyro.init()){\n'+
    ' while(1){\n'+
    '   Serial.println("Failed to detect gyro.");\n'+
    '   delay(2000);\n'+
    ' }\n'+
    '}\n'+
    "gyro.enableDefault();\n"

  var code = 'gyro.g.'+dropdown_axis;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.zumo_mag = function() {
  var dropdown_axis = this.getFieldValue('axis');
  
    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

    Blockly.Arduino.definitions_['var_lsm303'] = "LSM303 compass;";

    Blockly.Arduino.setups_['compass_setup'] = 
    'if(!compass.init()){\n'+
    ' while(1){\n'+
    '   Serial.println(F("Failed to detect compass."));\n'+
    '   delay(2000);\n'+
    ' }\n'+
    '}\n'+
    "compass.enableDefault();\n"

  var code = 'compass.m.'+dropdown_axis;
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.zumo_button = function() {
  var dropdown_button = this.getFieldValue('button');

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_button_'+dropdown_button] = "Zumo32U4Button"+dropdown_button+" button"+dropdown_button+";";
  var code = "button"+dropdown_button+".isPressed()";
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.zumo_wait_for_button = function() {
  var dropdown_button = this.getFieldValue('button');

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_button_'+dropdown_button] = "Zumo32U4Button"+dropdown_button+" button"+dropdown_button+";";
  var code = "button"+dropdown_button+".waitForButton()";
  return code;
};


Blockly.Arduino.zumo_buzzer = function() {
  var note = Blockly.Arduino.valueToCode(this, 'note', Blockly.Arduino.ORDER_ATOMIC);
  var duration = Blockly.Arduino.valueToCode(this, 'duration', Blockly.Arduino.ORDER_ATOMIC);
  var volume = this.getFieldValue('volume');

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
  Blockly.Arduino.definitions_['var_buzzer'] = "Zumo32U4Buzzer buzzer;";

  var code = 'buzzer.playNote(' + note + ', ' + duration + ', ' + volume + ');\n';
  return code;
};

Blockly.Arduino.zumo_buzzer_stop = function() {

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_buzzer'] = "Zumo32U4Buzzer buzzer;";

  var code = 'buzzer.stopPlaying();\n';
  return code;
};

Blockly.Arduino.zumo_buzzer_isplaying = function() {
  Blockly.Arduino.definitions_['var_buzzer'] = "Zumo32U4Buzzer buzzer;";

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  var code = 'buzzer.isPlaying()';
  
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.zumo_line_full = function() {
  var dropdown_name = this.getFieldValue('sensor_name');

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_line_full'] = "Zumo32U4LineSensors lineSensors;";
  Blockly.Arduino.definitions_['var_line_sensor_reading'] = "uint16_t lineSensorValues[5];";
  
  Blockly.Arduino.setups_['line_sensor_setup'] = 'lineSensors.initFiveSensors();\n lineSensors.calibrate(QTR_EMITTERS_ON);';

  Blockly.Arduino.definitions_['func_read_line_sensor'] = "\n\nint readLineSensor(short i){\n"+
  "   lineSensors.read(lineSensorValues,QTR_EMITTERS_ON);\n"+
  "   return lineSensor[i];\n"+
  "}\n \n"

  var code = 'readLineSensor('+dropdown_name+')';
  
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.zumo_line = function(block) {
  var dropdown_sensor = block.getFieldValue('sensor_name');
  var colour_color = block.getFieldValue('colour');

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
  
  Blockly.Arduino.definitions_['var_line_full'] = "Zumo32U4LineSensors lineSensors;";
  Blockly.Arduino.definitions_['var_line_sensor_reading'] = "uint16_t lineSensorValues[5];";
  
  Blockly.Arduino.setups_['line_sensor_setup'] = 'lineSensors.initFiveSensors();\n lineSensors.calibrate(QTR_EMITTERS_ON);';

  Blockly.Arduino.definitions_['func_read_line_sensor'] = "\n\nint readLineSensor(short i){\n"+
  "   lineSensors.read(lineSensorValues,QTR_EMITTERS_ON);\n"+
  "   return lineSensor[i];\n"+
  "}\n \n"

  var comp;
  if(colour_color=='#000000'){
    comp = '<=';
  }else {
    comp = '>=';
  }

  var code = 'readLineSensor('+dropdown_sensor+') ' + comp + ' 500';
  
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.zumo_lcd_print = function() {
  var value_line_1 = Blockly.Arduino.valueToCode(this, 'line_1', Blockly.Arduino.ORDER_ATOMIC);
  var value_line_2 = Blockly.Arduino.valueToCode(this, 'line_2', Blockly.Arduino.ORDER_ATOMIC);

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_zumo_lcd'] = "Zumo32U4LCD lcd;";

  Blockly.Arduino.definitions_['func_lcd_print'] = "\n\nint lcd_print(char *line_1, char *line_2){\n"+
  "   lcd_clear();\n"+
  '   lcd.print('+ value_line_1 +');\n'+
  '   lcd.gotoXY(0,1);\n'+
  '   lcd.print('+ value_line_2 +');\n'+
  "}\n \n"
  return "lcd_print("+value_line_1 +","+value_line_2+");\n";
};

Blockly.Arduino.zumo_battery_voltage  = function() {
  
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  var code = 'readBatteryMillivolts()';
  
  return [code, Blockly.Arduino.ORDER_NONE];
};


Blockly.Arduino.zumo_usb_power = function() {
  
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  var code = 'usbPowerPresent()';

  return [code, Blockly.Arduino.ORDER_NONE];
};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

Blockly.Arduino.zumo_proximity_sensor = function() {
  var dropdown_sensor = this.getFieldValue('sensor');
  
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_zumo_proxsensor'] = "Zumo32U4ProximitySensors proxSensors;";
  Blockly.Arduino.setups_['proximity_setup'] = "proxSensors.initThreeSensors();";

  var code = 'proxSensors.readBasic' + dropdown_sensor.capitalize() + '()';
 
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.zumo_encoder = function() {
  

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_zumo_encoder'] = "Zumo32U4Encoders encoders;";

  var code = 'encoders.getCounts'+dropdown_lr.capitalize()+'()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.zumo_encoder_reset = function() {
  var dropdown_lr = this.getFieldValue('lr');

  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_zumo_encoder'] = "Zumo32U4Encoders encoders;";

  var code = 'encoders.getCountsAndReset'+dropdown_lr.capitalize()+'()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.zumo_motor_test = function() {
    var value_l_motor = Blockly.Arduino.valueToCode(this, 'l_motor', Blockly.Arduino.ORDER_ATOMIC) || '0';
    var value_r_motor = Blockly.Arduino.valueToCode(this, 'r_motor', Blockly.Arduino.ORDER_ATOMIC) || '0';

    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

    Blockly.Arduino.definitions_['var_zumo32u4motors'] = "Zumo32U4Motors motors;\n";
    var code = "motors.setSpeeds("+value_l_motor+"," + value_r_motor + ");\n";
    return code;
};


Blockly.Arduino.zumo_forward = function() {
    var value_motor_speed = Blockly.Arduino.valueToCode(this, 'motor_speed', Blockly.Arduino.ORDER_ATOMIC) || '0';
    var value_motor_time = Blockly.Arduino.valueToCode(this, 'motor_time', Blockly.Arduino.ORDER_ATOMIC) || '0';

    Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
    Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

    Blockly.Arduino.definitions_['var_zumo32u4motors'] = "Zumo32U4Motors motors;\n";
    var code = "motors.setSpeeds("+value_motor_speed+"," + value_motor_speed + ");\ndelay("+value_motor_time+");\nmotors.setSpeeds(0, 0);\n";
    return code;
};



Blockly.Arduino.intermediate_motor_control = function() {
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";
  Blockly.Arduino.definitions_['var_zumo32u4motors'] = "Zumo32U4Motors motors;\n";

  var value_left_motor = Blockly.Arduino.valueToCode(this, 'left_motor', Blockly.Arduino.ORDER_ATOMIC) || '0';
  var value_right_motor = Blockly.Arduino.valueToCode(this, 'right_motor', Blockly.Arduino.ORDER_ATOMIC) || '0';
  
  // make sure value is within acceptable range -400 to 400
  value_left_motor = value_left_motor.replace("(", "");
  value_left_motor = value_left_motor.replace(")", "");
  if (value_left_motor > 400) {
    value_left_motor = 400
  } else if (value_left_motor < -400){
    value_left_motor = -400
  }
  value_right_motor = value_right_motor.replace("(", "");
  value_right_motor = value_right_motor.replace(")", "");
  if (value_right_motor > 400 ){
    value_right_motor = 400
  } else if (value_right_motor < -400){
    value_right_motor = -400
  }

  var code = 'motors.setSpeeds(' + value_left_motor + ',' + value_right_motor + ');\n';
  return code;
};

Blockly.Arduino.intermediate_delay = function() {
  var value_delay_time = Blockly.Arduino.valueToCode(this, 'delay_time', Blockly.Arduino.ORDER_ATOMIC) || '0';
  
  // make sure value is positive
  value_delay_time = value_delay_time.replace("(", "");
  value_delay_time = value_delay_time.replace(")", "");
  if (value_delay_time < 0 ){
    value_delay_time = 0
  }
  var code = 'delay(' + value_delay_time + ');\n';
  return code;
};