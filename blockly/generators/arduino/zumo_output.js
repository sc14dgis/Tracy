'use strict';

goog.provide('Blockly.Arduino.zumo_output');

goog.require('Blockly.Arduino');



Blockly.Arduino.buzzer_bach = function() {
// play the first few measures of Bach's fugue in D-minor (from zumo libary)
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4Buzzer.h>";
  Blockly.Arduino.definitions_['var_buzzer'] = "Zumo32U4Buzzer buzzer;";

  var code = 'buzzer.play("!T240 L8 agafaea dac+adaea fa<aa<bac#a dac#adaea f4");';
  return code;
};
Blockly.Arduino.buzzer_play_frequency = function() {
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4Buzzer.h>";
  Blockly.Arduino.definitions_['var_buzzer'] = "Zumo32U4Buzzer buzzer;";

  var value_frequency = Blockly.Arduino.valueToCode(this, 'frequency', Blockly.Arduino.ORDER_ATOMIC);
  var value_duration = Blockly.Arduino.valueToCode(this, 'duration', Blockly.Arduino.ORDER_ATOMIC);
  var value_volume = Blockly.Arduino.valueToCode(this, 'volume', Blockly.Arduino.ORDER_ATOMIC);

  // ensure values are within expected peramaters
  if (value_frequency > 10000){
  	value_frequency = 10000;
  } else if (value_frequency < 40){
  	value_frequency = 40;
  }
  value_duration = value_duration.replace("(", "");
  value_duration = value_duration.replace(")", "");
  if (value_duration < 0){
  	value_duration = 0;
  }
  value_volume = value_volume.replace("(", "");
  value_volume = value_volume.replace(")", "");
  if (value_volume > 10){
	value_volume = 15;
  } else if (value_volume < 0){
  	value_volume = 0
  }

  var code = 'buzzer.playFrequency('+value_frequency+','+value_duration+','+value_volume+');\n';
  return code;
};

Blockly.Arduino.lcd_set_text_top = function() {
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_lcd'] = "Zumo32U4LCD lcd;";

  var value_text = Blockly.Arduino.valueToCode(this, 'text', Blockly.Arduino.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'lcd.gotoXY(0,0);\nlcd.print('+ value_text +');\n';
  return code;
};

Blockly.Arduino.lcd_set_text_lower = function() {
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_lcd'] = "Zumo32U4LCD lcd;";

  var value_text = Blockly.Arduino.valueToCode(this, 'text', Blockly.Arduino.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'lcd.gotoXY(0,1);\nlcd.print('+ value_text +');\n';
  return code;
};

Blockly.Arduino.lcd_clear_screen = function() {
  Blockly.Arduino.definitions_['define_wire'] = "#include <Wire.h>";
  Blockly.Arduino.definitions_['define_zumo34u4'] = "#include <Zumo32U4.h>";

  Blockly.Arduino.definitions_['var_lcd'] = "Zumo32U4LCD lcd;";
  var code = 'lcd.clear();\n';
  return code;
};