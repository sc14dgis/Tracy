goog.provide('Blockly.Blocks.zumo');

goog.require('Blockly.Blocks');

Blockly.Blocks['zumo_motors'] = {
  init: function() {
    this.appendValueInput("l_motor")
        .setCheck("Number")
        .appendField("Left motor");
    this.appendValueInput("r_motor")
        .setCheck("Number")
        .appendField("Right motor");
    this.setInputsInline(true);
    this.setPreviousStatement(true,null);
    this.setNextStatement(true,null);
    this.setColour(260);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_stop'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Stop Tracy");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(260);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_accel'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Accelerometer: ")
        .appendField(new Blockly.FieldDropdown([["X", "x"], ["Y", "y"], ["Z", "z"]]), "axis")
        .appendField("-axis");
    this.setInputsInline(true);
    this.setOutput(true,'Number');
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_gyro'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Gyroscope: ")
        .appendField(new Blockly.FieldDropdown([["X", "x"], ["Y", "y"], ["Z", "z"]]), "axis")
        .appendField("-axis");
    this.setInputsInline(true);
    this.setOutput(true,'Number');
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Blocks['zumo_mag'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Magnetometer: ")
        .appendField(new Blockly.FieldDropdown([["X", "x"], ["Y", "y"], ["Z", "z"]]), "axis")
        .appendField("-axis");
    this.setInputsInline(true);
    this.setOutput(true,'Number');
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_button'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Button: ")
        .appendField(new Blockly.FieldDropdown([["A", "a"], ["B", "b"], ["C", "c"]]), "button")
        .appendField(" is pressed");
    this.setInputsInline(true);
    this.setOutput(true,'Number');
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_wait_for_button'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("wait for button ")
        .appendField(new Blockly.FieldDropdown([["A", "a"], ["B", "b"], ["C", "c"]]), "button")
        .appendField(" to be pressed");
    this.setInputsInline(true);
    this.setOutput(true,'Number');
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};


Blockly.Blocks['zumo_buzzer'] = {
  init: function() {
    this.appendValueInput("note")
        .appendField("Play");
    this.appendValueInput("duration")
        .appendField("for ");
    this.appendDummyInput()
        .appendField("seconds. Volume:")
        .appendField(new Blockly.FieldDropdown([["V. Loud", "15"], ["Loud", "12"], ["Normal", "9"], ["Quiet", "6"], ["V. Quiet", "3"]]), "volume");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_buzzer_stop'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Stop playing sounds.");
    this.setPreviousStatement(true,null);
    this.setNextStatement(true,null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_buzzer_isplaying'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Is playing sounds?");
    this.setOutput(true, "Boolean");
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};


Blockly.Blocks['zumo_line_full'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Read line sensor ")
        .appendField(new Blockly.FieldDropdown([["Left Corner", "0"], ["Left", "1"], ["Center", "2"], ["Right", "3"], ["Right Corner", "4"]]), "sensor_name");
    this.setInputsInline(true);
    this.setOutput(true, "Number");
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_line'] = {
  init: function() {
    var colour = new Blockly.FieldColour('#ffffff');
    colour.setColours(['#000','#fff']).setColumns(2);

    this.appendDummyInput()
        .appendField("Is ")
        .appendField(new Blockly.FieldDropdown([["Left Corner", "0"], ["Left", "1"], ["Center", "2"], ["Right", "3"], ["Right Corner", "4"]]), "sensor_name")
        .appendField("sensor ")
        .appendField(colour, "colour");
    this.setInputsInline(true);
    this.setOutput(true, "Boolean");
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_lcd_print'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Lcd display");
    this.appendValueInput("line_1")
        .setCheck(["Boolean", "Number", "String", "Array"])
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Line 1");
    this.appendValueInput("line_2")
        .setCheck(["Boolean", "Number", "String", "Array"])
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Line 2");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_battery_voltage'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Battery voltage");
    this.setOutput(true, "Number");
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_usb_power'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("powered from USB?");
    this.setOutput(true, "Boolean");
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_proximity_sensor'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["Front", "Front"], ["Left", "Left"], ["Right", "Right"]]), "sensor");
    this.appendDummyInput()
        .appendField("proximity sensor");
    this.setInputsInline(true);
    this.setOutput(true,null);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_encoder'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Read");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["Left", "left"], ["Right", "right"]]), "lr");
    this.appendDummyInput()
        .appendField("encoder");
    this.setInputsInline(true);
    this.setOutput(true, "Number");
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_encoder_reset'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Read");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["Left", "left"], ["Right", "right"]]), "lr");
    this.appendDummyInput()
        .appendField("encoder with reset");
    this.setInputsInline(true);
    this.setOutput(true, "Number");
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};


Blockly.Blocks['zumo_motor_test'] = {
  init: function() {
    this.appendValueInput("l_motor")
        .setCheck("Number")
        .appendField("Left motor");
    this.appendValueInput("r_motor")
        .setCheck("Number")
        .appendField("Right motor");
    this.setInputsInline(true);
    this.setPreviousStatement(true,null);
    this.setNextStatement(true,null);
    this.setColour(260);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['zumo_forward'] = {
	init: function() {
    	this.appendValueInput("motor_speed")
    	    .setCheck("Number")
    	    .appendField("Motor Speed");
    	this.appendValueInput("motor_time")
    	    .setCheck("Number")
    	    .appendField("Time to run");
    	this.setInputsInline(true);
    	this.setPreviousStatement(true,null);
    	this.setNextStatement(true,null);
    	this.setColour(260);
    	this.setTooltip('');
    	this.setHelpUrl('http://www.example.com/');
  }
};



Blockly.Blocks['intermediate_motor_control'] = {
  init: function() {
    this.appendValueInput("left_motor")
        .setCheck("Number", "Variable")
        .appendField("Set motor speed, Left:");
    this.appendValueInput("right_motor")
        .setCheck("Number", "Variable")
        .appendField("Right:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('Set speed of motors, from -400 to 400');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['intermediate_delay'] = {
  init: function() {
    this.appendValueInput("delay_time")
        .setCheck("Number")
        .appendField("Delay for :");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('Pause program for a time.');
    this.setHelpUrl('');
  }
};