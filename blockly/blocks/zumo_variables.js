'use strict';

goog.provide('Blockly.Blocks.zumo_variables');

goog.require('Blockly.Blocks');

Blockly.Blocks['setup_var'] = {
  init: function() {
    this.appendValueInput("NAME")
        .setCheck(null)
        .appendField("Create")
        .appendField(new Blockly.FieldVariable("item"), "var_name")
        .appendField("as")
        .appendField(new Blockly.FieldDropdown([["Integer","int"], ["String","char[]"]]), "var_type");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};