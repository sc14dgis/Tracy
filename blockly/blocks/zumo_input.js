'use strict';

goog.provide('Blockly.Blocks.zumo_input');

goog.require('Blockly.Blocks');

Blockly.Blocks['zumo_button_wait'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Wait till button")
        .appendField(new Blockly.FieldDropdown([["A","A"], ["B","B"], ["B","B"]]), "button_selection");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};


Blockly.Blocks['zumo_proximity_sensors'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Read From Sensors");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['left_prox_sensor'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Left Proximity Sensor Reading");
    this.setOutput(true, null);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['zumo_accelerometer'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Read From accelerometer");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['accel_x'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("accelerometer x axsis");
    this.setOutput(true, null);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['accel_y'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("accelerometer y axsis");
    this.setOutput(true, null);
    this.setColour(20);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};