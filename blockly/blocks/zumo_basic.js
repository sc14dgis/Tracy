'use strict';

goog.provide('Blockly.Blocks.zumo_basic');

goog.require('Blockly.Blocks');

Blockly.Blocks['basic_forward'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("arrowUp.png", 30, 30, "*"))
        .appendField("Forward");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
    this.setTooltip('Move Robot Forward');
    this.setHelpUrl('');
  }
};
Blockly.Blocks['basic_reverse'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("arrowDown.png", 30, 30, "*"))
        .appendField("Reverse");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
    this.setTooltip('Move Robot Backwards');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['basic_right_turn'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("arrowRight.png", 30, 30, "*"))
        .appendField("Right Turn");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
    this.setTooltip('Turn robot right.');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['basic_left_turn'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("arrowLeft.png", 30, 30, "*"))
        .appendField("Left Turn");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
    this.setTooltip('Turn robot left.');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['basic_loop'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("repeat.png", 30, 30, "*"))
        .appendField("Repeat")
        .appendField(new Blockly.FieldDropdown([["0","0"], ["1","1"], ["2","2"], ["3","3"], ["4","4"], ["5","5"], ["6","6"], ["7","7"], ["8","8"], ["9","9"], ["10","10"]]), "loop_count")
        .appendField("time(s):");
    this.appendStatementInput("statements_in_loop")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip('Repeat Loop');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['basic_start'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("flag.png", 30, 30, "*"))
        .appendField("Start");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(20);
    this.setTooltip('Start program with button press');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['basic_end'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("stop.png", 30, 30, "*"))
        .appendField("End program.");
    this.setPreviousStatement(true, null);
    this.setColour(20);
    this.setTooltip('End program.');
    this.setHelpUrl('');
  }
};