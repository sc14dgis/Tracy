'use strict';

goog.provide('Blockly.Blocks.zumo_unitTests');

goog.require('Blockly.Blocks');



Blockly.Blocks['run_tests'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("run tests");
    this.appendStatementInput("loop")
        .setCheck("unitTest");
   // this.setColour(65);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['test_vs_input'] = {
  init: function() {
    this.appendValueInput("Name")
        .setCheck(null)
        .appendField("Name");
    this.appendValueInput("Actual")
        .setCheck(null)
        .appendField("Actual");
    this.appendValueInput("Expected")
        .setCheck(null)
        .appendField("Expected");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['test_Text'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput("test_Text"), "Test");
    this.setOutput(true, null);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['statement_Test'] = {
  init: function() {
    this.appendValueInput("test_Name")
        .setCheck(null)
        .appendField("test name");
    this.appendStatementInput("statement_To_Test")
        .setCheck(null)
        .appendField("Actual");
    this.appendValueInput("expected_Output")
        .setCheck(null)
        .appendField("Expected");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};