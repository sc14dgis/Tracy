'use strict';

goog.provide('Blockly.Blocks.zumo_output');

goog.require('Blockly.Blocks');

Blockly.Blocks['buzzer_bach'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Buzzer: Play Bach");
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['buzzer_play_frequency'] = {
  init: function() {
    this.appendValueInput("frequency")
        .setCheck("Number")
        .appendField("Buzzer play, Frequency:");
    this.appendValueInput("duration")
        .setCheck("Number")
        .appendField("Duration:");
    this.appendValueInput("volume")
        .setCheck("Number")
        .appendField("Volume:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('play a sound with the buzzer (frequency (40-10,000Hz), duration(in miliseconds), volume (0-15)).');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['lcd_set_text_top'] = {
  init: function() {
    this.appendValueInput("text")
        .setCheck("String")
        .appendField("Set LCD text top line:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('only 8 charatcers on a line!');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['lcd_set_text_lower'] = {
  init: function() {
    this.appendValueInput("text")
        .setCheck("String")
        .appendField("Set LCD text bottom line:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('only 8 charatcers on a line!');
    this.setHelpUrl('');
  }
};

Blockly.Blocks['lcd_clear_screen'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Clear LCD screen");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
    this.setTooltip('');
    this.setHelpUrl('');
  }
};
